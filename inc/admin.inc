<?php
/**
 * @file
 *
 * Vkontakte login app settings.
 */
function vk_login_settings() {

  $form['vk_login_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('VK App Id'),
    '#default_value' => variable_get('vk_login_app_id'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['vk_login_app_key'] = array(
    '#type' => 'textfield',
    '#title' => t('VK App key'),
    '#default_value' => variable_get('vk_login_app_key'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  
  $form['vk_login_auto_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('VK App key'),
    '#options' => array(1, 2),
    '#default_value' => variable_get('vk_login_auto_register', '1'),
  );
  
  $form['vk_login_landing_page'] = array(
    '#type' => 'textfield',
    '#title' => t('VK login landing page'),
    '#description' => t('You can set different landing page. By default this is frontpage'),
    '#default_value' => variable_get('vk_login_landing_page', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
